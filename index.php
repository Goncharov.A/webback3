<?php
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');


if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!empty($_GET['save'])) {
      print('Спасибо, результаты сохранены.');
    }
    include('form.php');
    exit();
}
  


// Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['fio'])) {
      print('Заполните имя.<br/>');
      $errors = TRUE;
  }
  if (empty($_POST['email'])) {
      print('Заполните почту.<br/>');
      $errors = TRUE;
  }

  if (empty($_POST['data'])) {
    print('Заполните дату рождения.<br/>');
    $errors = TRUE;
  }
    if (!isset($_POST['radio1'])) {
      print('Выберите свой пол.<br/>');
      $errors = TRUE;
  }

  if (!isset($_POST['radio2'])) {
    print('Выберите количество конечностей.<br/>');
    $errors = TRUE;
}

if (empty($_POST['talents'])) {
  print('Выберите талант.<br/>');
  $errors = TRUE;
}

  if (empty($_POST['biography'])) {
      print('Заполните биографию.<br/>');
      $errors = TRUE;
  }
  if (!isset($_POST['checkbox'])) {
      print('Вы должны быть согласны с условиями.<br/>');
      $errors = TRUE;
  }

  if ($errors) {
      exit();
  }
$name = $_POST['fio'];
$email = $_POST['email'];
$data = $_POST['data'];
$gender = $_POST['radio1'];
$limbs = $_POST['radio2'];
$bio = $_POST['biography'];

//Сохранение в базу данных.
 $db = new PDO('mysql:host=localhost;dbname=u35649', 'u35649', '35734534', array(PDO::ATTR_PERSISTENT => true));
 //Подготовленный запрос. Не именованные метки.
  $user = $db->prepare("INSERT INTO user set name = ?, email = ?, date = ?, gender = ?, amountOFColumn = ?, biography = ?");
  $user -> execute([$name, $email, $data, $gender, $limbs, $bio]);
  $userId = $db->lastInsertId();
  
  $talents = array();
  $talents = $_POST['talents'];

    $userPower = $db->prepare("INSERT INTO userPower (userId, powerId) VALUES (:userId, :powerId)");
    foreach($talents as $ability) {
      $userPower -> execute(array('userId' => $userId, 'powerId' => $ability));
    }
  

header('Location: ?save=1');
?>

